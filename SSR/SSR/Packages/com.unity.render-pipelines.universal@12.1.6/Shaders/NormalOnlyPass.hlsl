#ifndef UNIVERSAL_DEPTH_ONLY_PASS_INCLUDED
#define UNIVERSAL_DEPTH_ONLY_PASS_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

struct Attributes
{
	float4 positionOS : POSITION;
	float3 normalOS : NORMAL;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
	float4 positionCS : SV_POSITION;
	float3 normalWS : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};

	Varyings NormalOnlyVertex(Attributes input)
	{
		Varyings output = (Varyings) 0;
		UNITY_SETUP_INSTANCE_ID(input);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

		output.positionCS = TransformObjectToHClip(input.positionOS.xyz);
		output.normalWS = TransformObjectToWorldNormal(input.normalOS.xyz); // ワールド法線

		return output;
	}

	half4 NormalOnlyFragment(Varyings input) : SV_TARGET
	{
		UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

		float3 normal = normalize(input.normalWS.xyz); //一応正規化
		/*ワールド法線をRGBに変換*/
		normal = normal * 0.5f + 0.5f; //-1~1を0〜1に変換
        
		return half4(normal, 1);
	}
#endif