using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using System;

/// <summary>
/// 法線情報をレンダリングするレンダリングパス
/// </summary>
public class NormalOnly : ScriptableRendererFeature
{
    /// <summary>
    /// レンダリングパス
    /// </summary>
    class CustomRenderPass : ScriptableRenderPass
    {
        /// <summary>
        /// レンダリングパス上の名前
        /// </summary>
        private const string _profilerTag = "NormalOnlyPass";

        /// <summary>
        /// サンプラー生成
        /// </summary>
        private ProfilingSampler _profilingSampler = new ProfilingSampler(_profilerTag);

        /// <summary>
        /// 同タグを持つシェーダパスを実行する
        /// </summary>
        private ShaderTagId _shaderTagID = new ShaderTagId("NormalOnly");

        /// <summary>
        /// フィルタリング設定
        /// </summary>
        private FilteringSettings _filteringSettings;

        /// <summary>
        /// レンダリング先のテクスチャ識別子
        /// </summary>
        private RenderTargetHandle _textureHandle;

        /// <summary>
        /// レンダリング先テクスチャの情報格納先
        /// </summary>
        private RenderTextureDescriptor _descriptor;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="settings"> レンダリングタイミング・レイヤーマスク </param>
        /// <param name="renderQueueRange"> レンダリング対象 </param>
        public CustomRenderPass(NormalOnlyFeatureSettins settings, RenderQueueRange renderQueueRange)
        {
            this._filteringSettings = new FilteringSettings(renderQueueRange, settings._layerMask);
            renderPassEvent = settings._renderPassEvent;
        }

        /// <summary>
        /// 手動初期化
        /// </summary>
        /// <param name="baseDescriptor"> テクスチャ設定 </param>
        /// <param name="setAttachmentHandle"> テクスチャハンドル </param>
        public void SetUp(RenderTextureDescriptor baseDescriptor, RenderTargetHandle setAttachmentHandle)
        {
            this._textureHandle = setAttachmentHandle;
            baseDescriptor.colorFormat = RenderTextureFormat.ARGB32;    //フォーマット指定
            baseDescriptor.depthBufferBits = 32;    //Zテストに32bit必要らしい

            baseDescriptor.msaaSamples = 1; //MSAAは使用しない
            this._descriptor = baseDescriptor;
        }

        /// <summary>
        /// レンダリング前に呼ばれる
        /// </summary>
        /// <param name="cmd"> コマンドバッファ(実行する描画処理リスト) </param>
        /// <param name="cameraTextureDescriptor"> レンダリング先テクスチャ生成設定(使わない) </param>
        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            cmd.GetTemporaryRT(this._textureHandle.id, _descriptor, FilterMode.Point);
            ConfigureTarget(this._textureHandle.Identifier());
            ConfigureClear(ClearFlag.All, Color.black);
        }

        /// <summary>
        /// 実行
        /// </summary>
        /// <param name="context"> 描画コマンド </param>
        /// <param name="renderingData"> カメラ情報とかいろいろ入っているらしい </param>
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            /*コマンド初期化*/
            CommandBuffer cmd = CommandBufferPool.Get(_profilerTag);
            cmd.Clear();

            /*設定*/
            var sortFlags = renderingData.cameraData.defaultOpaqueSortFlags;
            var drawSettings = CreateDrawingSettings(this._shaderTagID, ref renderingData, sortFlags);
            drawSettings.perObjectData = PerObjectData.None;

            /*実行*/
            context.DrawRenderers(renderingData.cullResults, ref drawSettings, ref this._filteringSettings);
            context.ExecuteCommandBuffer(cmd);

            /*解放*/
            CommandBufferPool.Release(cmd);
        }

        /// <summary>
        /// レンダリング終了後の開放処理など
        /// </summary>
        /// <param name="cmd"> 描画コマンド </param>
        /// <exception cref="ArgumentNullException"></exception>
        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("NormalOnly:cmd null");

            /*レンダリング先テクスチャ解放*/
            if (this._textureHandle != RenderTargetHandle.CameraTarget)
            {
                cmd.ReleaseTemporaryRT(this._textureHandle.id);
                this._textureHandle = RenderTargetHandle.CameraTarget;
            }
        }
    }

    /// <summary>
    /// Enum
    /// </summary>
    public class NormalOnlyFeatureSettins
    {
        public RenderPassEvent _renderPassEvent = RenderPassEvent.AfterRenderingOpaques;
        public LayerMask _layerMask = -1;
    }

    /// <summary>
    /// パス設定
    /// </summary>
    [SerializeField] NormalOnlyFeatureSettins _settings = new NormalOnlyFeatureSettins();

    /// <summary>
    /// レンダリングパス(上記クラス)
    /// </summary>
    CustomRenderPass _normalOnlyPass = null;

    /// <summary>
    /// レンダーターゲット(レンダリング先テクスチャ)
    /// </summary>
    private RenderTargetHandle _normalTexture;

    /// <summary>
    /// 初期化
    /// </summary>
    public override void Create()
    {
        this._normalOnlyPass = new CustomRenderPass(_settings, RenderQueueRange.opaque);
        this._normalTexture.Init("_NormalOnlyTexture");
    }

    /// <summary>
    /// レンダリングパス追加
    /// </summary>
    /// <param name="renderer"> render </param>
    /// <param name="renderingData"> renderingData </param>
    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        var cameraData = renderingData.cameraData;

        if (cameraData.isPreviewCamera || cameraData.isSceneViewCamera)
            return;

        var cameraTargetDescriptor = renderingData.cameraData.cameraTargetDescriptor;
        this._normalOnlyPass.SetUp(cameraTargetDescriptor, this._normalTexture);

        renderer.EnqueuePass(this._normalOnlyPass);
    }
}