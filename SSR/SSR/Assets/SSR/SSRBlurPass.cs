using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/// <summary>
/// SSRにブラーをかける処理(最終レンダリング)
/// </summary>
public class SSRBlurPass : ScriptableRendererFeature
{
    /// <summary>
    /// レンダリングパス
    /// </summary>
    class CustomRenderPass : ScriptableRenderPass
    {
        /// <summary>
        /// レンダーターゲットの識別子
        /// </summary>
        private readonly int RenderTargetTexId = Shader.PropertyToID("_SSRBlurRT");

        /// <summary>
        /// 現在のレンダリング状態
        /// </summary>
        public RenderTargetIdentifier _currentRenderTarget;

        /// <summary>
        /// SSRブラーを適用したマテリアル
        /// </summary>
        public Material _material;

        /// <summary>
        /// レンダリング
        /// </summary>
        /// <param name="context"></param>
        /// <param name="renderingData"></param>
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            /*バッファプールを取得*/
            var commandBuffer = CommandBufferPool.Get("SSR Blur Pass");
            /*レンダリング中のカメラデータ*/
            var cameraData = renderingData.cameraData;  

            /*レンダリング先のテクスチャサイズ*/
            var w = cameraData.camera.scaledPixelWidth;
            var h = cameraData.camera.scaledPixelHeight;

            /*仮想描画先を取得*/
            commandBuffer.GetTemporaryRT(RenderTargetTexId, w, h);

            /*マテリアルに適用されているシェーダを実行*/
            commandBuffer.Blit(cameraData.targetTexture, RenderTargetTexId, this._material);
            
            /*レンダリングした結果をバックバッファに適用*/
            commandBuffer.Blit(RenderTargetTexId, this._currentRenderTarget);

            /*仮想描画先を破棄*/
            commandBuffer.ReleaseTemporaryRT(RenderTargetTexId);

            /*実行順序をスケジュール・実行*/
            context.ExecuteCommandBuffer(commandBuffer);
            context.Submit();

            /*バッファプールを破棄*/
            CommandBufferPool.Release(commandBuffer);
        }
    }

    /// <summary>
    /// 描画パス
    /// </summary>
    private CustomRenderPass _renderPass;

    /// <summary>
    /// シェーダを格納したマテリアル
    /// </summary>
    [SerializeField] private Material _material;

    /// <summary>
    /// 初期化
    /// </summary>
    public override void Create()
    {
        _renderPass = new CustomRenderPass();   //パス生成
        _renderPass._material = _material;  //マテリアルを格納
        _renderPass._material.color = Color.white;  //マテリアルカラー初期化
        _renderPass.renderPassEvent = RenderPassEvent.AfterRenderingSkybox; //スカイボックス描画後に上書き
    }

    /// <summary>
    /// レンダリング前に呼び出される(毎フレーム)
    /// </summary>
    /// <param name="renderer"> renderer </param>
    /// <param name="renderingData"> renderingData </param>
    public override void AddRenderPasses(ScriptableRenderer renderer,
        ref RenderingData renderingData)
    {
        _renderPass._currentRenderTarget = renderer.cameraColorTarget;  //レンダーターゲットを格納
        renderer.EnqueuePass(_renderPass);
    }
}


