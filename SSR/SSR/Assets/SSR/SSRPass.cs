using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/// <summary>
/// ＳＳＲを行うパス
/// </summary>
public class SSRPass : ScriptableRendererFeature
{
    /// <summary>
    /// レンダリングパス
    /// </summary>
    public class CustomRenderPass : ScriptableRenderPass
    {
        /// <summary>
        /// ＳＳＲマテリアル
        /// </summary>
        public Material _material;

        /// <summary>
        /// ＳＳＲテクスチャ
        /// </summary>
        private RenderTargetHandle _ssrTexture;

        /// <summary>
        /// デスクリプタ(テクスチャの情報とかだった気がする SRV,CBV,UAVなど)
        /// </summary>
        private RenderTextureDescriptor _descriptor;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="material"> マテリアル </param>
        /// <param name="evt"> レンダリングタイミング </param>
        public CustomRenderPass(Material material, RenderPassEvent evt)
        {
            this._material = material;
            this._material.color = Color.white;
            this.renderPassEvent = evt;
        }

        /// <summary>
        /// 手動初期化
        /// </summary>
        /// <param name="baseDescriptor"> baseDescriptor </param>
        /// <param name="setDepthAttachmentHandle"></param>
        public void SetUp(RenderTextureDescriptor baseDescriptor, RenderTargetHandle setDepthAttachmentHandle)
        {
            /*テクスチャ情報格納*/
            _ssrTexture = setDepthAttachmentHandle;

            baseDescriptor.colorFormat = RenderTextureFormat.ARGB32;    //フォーマット指定
            baseDescriptor.depthBufferBits = 32;    //Zテストに32bit必要らしい

            baseDescriptor.msaaSamples = 1; //MSAAは使用しない
            _descriptor = baseDescriptor;
        }

        /// <summary>
        /// レンダリング開始前呼び出し
        /// </summary>
        /// <param name="cmd"> commandbuffer </param>
        /// <param name="cameraTextureDescriptor"> cameraTextureDescriptor </param>
        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            /*一時テクスチャ取得*/
            cmd.GetTemporaryRT(_ssrTexture.id, _descriptor, FilterMode.Point);
        }

        /// <summary>
        /// レンダリング実行
        /// </summary>
        /// <param name="context"> 描画コマンド </param>
        /// <param name="renderingData"> レンダリングデータ </param>
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            /*コマンドバッファ生成*/
            var commandBuffer = CommandBufferPool.Get("SSR Pass");

            /*カメラデータ*/
            var cameraData = renderingData.cameraData;  

            /*VP行列・VP逆行列を転送*/
            var view = cameraData.camera.worldToCameraMatrix;
            var proj = GL.GetGPUProjectionMatrix(cameraData.camera.projectionMatrix, false);
            var viewProj = proj * view;
            _material.SetMatrix("_ViewProj", viewProj);
            _material.SetMatrix("_ViewProjInv", viewProj.inverse);

            /*現在の描画結果を一時テクスチャにコピーしてシェーダを実行*/
            ConfigureTarget(this._ssrTexture.Identifier());
            ConfigureClear(ClearFlag.All, Color.black);
            commandBuffer.Blit(this._ssrTexture.Identifier(), this._ssrTexture.Identifier(), this._material);

            /*実行順序をスケジュール・実行*/
            context.ExecuteCommandBuffer(commandBuffer);
            context.Submit();

            CommandBufferPool.Release(commandBuffer);   //バッファプールを破棄
        }

        /// <summary>
        /// レンダリング終了呼び出し
        /// </summary>
        /// <param name="cmd"></param>
        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
            {
                Debug.LogError("SSR PASS CMD NULL");
                return;
            }

            if (_ssrTexture != RenderTargetHandle.CameraTarget)
            {
                /*テクスチャを解放*/
                cmd.ReleaseTemporaryRT(_ssrTexture.id);
            }
        }
    }

    /// <summary>
    /// 描画パス
    /// </summary>
    private CustomRenderPass _ssrPass;

    /// <summary>
    /// シェーダを適用したマテリアル
    /// </summary>
    [SerializeField] private Material _material;

    /// <summary>
    /// SSRレンダリング先テクスチャ
    /// </summary>
    private RenderTargetHandle _ssrTexture;

    /// <summary>
    /// 初期化
    /// </summary>
    public override void Create()
    {
        _ssrPass = new CustomRenderPass(_material, RenderPassEvent.AfterRenderingSkybox);   //パス生成
        _ssrTexture.Init("_SSRTex");
    }

    /// <summary>
    /// レンダリング前に呼ばれる(毎フレーム)
    /// </summary>
    /// <param name="renderer"> render </param>
    /// <param name="renderingData"> renderingData </param>
    public override void AddRenderPasses(ScriptableRenderer renderer,
        ref RenderingData renderingData)
    {
        _ssrPass.SetUp(renderingData.cameraData.cameraTargetDescriptor, _ssrTexture);
        renderer.EnqueuePass(_ssrPass);
    }
}