
void ScreenSpaceReflection_float(
float3 origin, //レイマーチングの原点
float3 rayVec, //レイの反射方向
int maxLoop, //最大ループ数
float maxRayLength, //最大マーチング距離
float rayHitThickness, //レイ衝突の際の厚み
float maxCameraDistance,	//カメラからの最大距離
float reflectPower, //反射力
float4x4 MatrixVP, //CPU側で生成したGPU対応のViewProjection行列
float4x4 matrixInvVP, //上記VP行列の逆行列
UnityTexture2D depthTexture, //深度値格納テクスチャ
UnityTexture2D sceneTexture, //シーンレンダリング結果
out float3 OutColor)
{
	OutColor = float3(0, 0, 0); //出力色初期化
	
	float3 ray = origin; //座標初期化
	float marchingStep = maxRayLength / maxLoop; //一度のステップで動く距離
	
	/*レイマーチング開始*/
    [unroll(50)]
	for (int i = 0; i < maxLoop; i++)
	{
		ray += rayVec * marchingStep; //レイを進める
		
		/*カメラから一定距離離れていたら終了*/
		if (distance(_WorldSpaceCameraPos, ray) > maxCameraDistance)
			return;
		
		/*ビューポート座標*/
		float4 frustumPos = mul(MatrixVP, float4(ray, 1.0f));
		
		/*スクリーン座標*/
		float2 screenPos = (frustumPos.xy / (frustumPos.w + 0.0000001f)) * 0.5f + 0.5f;
		
		/*レイがスクリーン座標を超えた場合反射に破綻が生じるため終了*/
		if (screenPos.x > 1.0f || screenPos.x < 0.0f || screenPos.y > 1.0f || screenPos.y < 0.0f)
			return;
		
       /*深度テクスチャから深度を取得 手前が1奥が0*/
		float depth = tex2D(depthTexture, screenPos).r;
		
		/*カメラからレイまでの深度を計算 手前が1奥が0*/		
		float rayDepth = frustumPos.z / (frustumPos.w + 0.0000001f);
		
		/*そのピクセルの深度が、レイの深度よりも手前なら衝突
		＆
		深度の差が一定以下の場合のみ衝突*/
		if (depth > rayDepth && depth - rayDepth < rayHitThickness)
		{
			OutColor = tex2D(sceneTexture, screenPos).rgb * reflectPower; //レンダリング結果をサンプリング * 反射の強さ
			return;
		}
	}
}


