
// Precision : float
void ReconstructWorldFromDepth_float(
float2 ScreenPosition, 
float Depth, 
float4x4 matrixVPInv, 
out float3 Out)
{
    // スクリーン座標と深度値からカメラ座標系をに変換
    float4 clipPos = float4(ScreenPosition * 2.0 - 1.0, Depth, 1.0);

    #if !UNITY_UV_STARTS_AT_TOP
        clipPos.y = -clipPos.y;
    #endif

    // カメラ座標系に逆VP行列を適用し、ワールド座標にする    
    float4 worldPos = mul(matrixVPInv, clipPos);
	
    // 同次座標系を標準座標系に戻す
    Out = worldPos.xyz / worldPos.w;
}