
void SSRBlur_float(
float2 screenPos, //スクリーン座標
float offset, //サンプリングオフセット
int samplingCount, //サンプリング数
UnityTexture2D SSRTexture, //SSRの映りこみを描画したテクスチャ
out float3 OutColor)
{
	/*出力色初期化*/
	OutColor = 0;
	
	/*周辺4ピクセル*カウント数をサンプリングする*/
	for (int i = 1; i <= samplingCount; i++)
	{
		OutColor += tex2D(SSRTexture, screenPos + float2(-offset * i, -offset * i)).rgb;
		OutColor += tex2D(SSRTexture, screenPos + float2(-offset * i, offset * i)).rgb;
		OutColor += tex2D(SSRTexture, screenPos + float2(offset * i, -offset * i)).rgb;
		OutColor += tex2D(SSRTexture, screenPos + float2(offset * i, offset * i)).rgb;
	}
	
	/*平均値を求める*/
	OutColor = OutColor / (samplingCount * 4);
}